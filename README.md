# Welcome to Graphql App  !!

## Getting started

# Steps to follow for development on Local
You need backend running on your local. To do that follow these steps


1. Clone backend repo(https://gitlab.com/maheshbhalala27/graphql-demo)
I
2. install all dependencies - yarn 
3. Start the server yarn run start


This should run the development backend server on the port 4080 Confirm it by running the curl command on local. Find more details here.

**Now it's time to setup frontend.**

# Clone this repo

- Install all dependencies run yarn
Create .env file in the root directory.
Add this entry `REACT_APP_API_BASE_URL=http://localhost:4080/graphqlin` .env file.

- Start the server - yarn run start
During development, your backend api's base url will be `http://localhost:4080/graphql`At the time of deployment it will be required to change.

- If you don't want to setup backend repo on your local then directly put the URL of backend service as the value of REACT_APP_API_BASE_URL.

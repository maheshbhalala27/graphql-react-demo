import React from 'react'

const Footer = () => {
  return (
    <div className='footer'>
       <p className="mt-4">© Copyright 2023. demo studio. All Rights Reserved.</p>
    </div>
  )
}

export default Footer

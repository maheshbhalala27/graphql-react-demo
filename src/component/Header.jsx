import React from 'react'
import { Link } from 'react-router-dom'
import  LogoImage  from '../assets/image/graphql-logo.jfif'

const Header = () => {
  return (
    <>
       <header className="App-header">
          <div  className="gpq-logo">
          <Link to={"/"} >
            <img src={LogoImage}  />
          </Link>
          </div>
          <div className="">
            <ul className="list-header">
              <li>
                <Link to={"/"} data-testid="home">Home</Link>
              </li>
              <li>
                <Link to={"create"} data-testid="create" >Create</Link>
              </li>
              <li>
                <Link to={"subscription"} data-testid="subscription" >Subscription</Link>
              </li>
              {/* <li>
                <Link to={"get-content"} data-testid="content" >Content</Link>
              </li> */}
            </ul>
          </div>
        </header>
    </>
  )
}

export default Header

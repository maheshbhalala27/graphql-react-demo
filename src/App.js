import Router from "./Router";
import "../src/assets/css/App.css";

function App() {
  return (
    <>
        <Router />
    </>
  );
}

export default App;

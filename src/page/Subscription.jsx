import React, { useEffect, useState } from 'react';
import { gql, useSubscription } from '@apollo/client';
import { client } from '../store/client';

const Subscription = () => {

  const NEW_MESSAGE = gql`
  subscription {
    newMessage {
      id
      content
    }
  }
`;

  const [messages, setMessages] = useState([]);

  const { data, error } = useSubscription(NEW_MESSAGE,);

  useEffect(() => {
    if (data) {
      setMessages((prevMessages) => [...prevMessages, data.newMessage]);
    }
  }, [data]);

  const postMessage = async (content) => {
    await client.mutate({
      mutation: gql`
        mutation ($content: String!) {
          postMessage(content: $content)
        }
      `,
      variables: {
        content,
      },
    });
  };

  // if (error) return `Error...! ${error.message}`;

  return (
    <>
      <form style={{ margin: '25px', display: "flex", alignItems: "center", justifyContent: "center" }}
        onSubmit={(e) => {
          if (e.target.elements.content.value !== "" && e.target.elements.content.value !== undefined) {
            e.preventDefault();
            const content = e.target.elements.content.value;
            postMessage(content);
            e.target.elements.content.value = '';
          }
        }}
      >
        <label htmlFor="content">Content</label>
        <input type="text" className='input-content' name="content" id='content' />
        <button type="submit" data-testid="send-button"  >Send</button>
      </form>

        <h2 style={{textAlign:"center"}}>Message List</h2>
      <div className='' style={{ margin: '25px', display: "flex", alignItems: "center", justifyContent: "center" }}>
        <ul>
          {messages?.map((message) => (
            <li key={message?.id} data-testid="test-message" >{message?.content}</li>
          ))}
        </ul>
      </div>
    </>
  );
}

export default Subscription
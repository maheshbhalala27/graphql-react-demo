import React from 'react'
import { useQuery } from '@apollo/client';
import { GET_ALL_USER } from '../store/query';

const Home = () => {
  const { loading, error, data } = useQuery(GET_ALL_USER,{  fetchPolicy: "no-cache" });

  if (loading)
    return (
      <>
        <p>Loading...</p>
      </>
    );
  if (error) return <p>Error : {error.message}</p>;

  return (
    <div>
      {data?.userData?.map((e, i) => {
        return (
          <div key={i} style={{ margin: "20px 30px" }}>
            <b>Id :</b><span> {e.id}</span>
            <p className='first-name'>
              <b> First Name : </b>
              <span>
                {e.FirstName}
              </span>
            </p>
            <p>
              <b>
                Last Name : </b>
                <span>
              {e.lastName}
              </span>
            </p>
          </div>
        );
      })}
    </div>
  );
}

export default Home
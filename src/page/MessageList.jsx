import React from 'react';
import { useSubscription, gql } from '@apollo/client';



const MessageList = () => {

    const NEW_MESSAGE = gql`
subscription {
  newMessage {
    id
    content
  }
}
`;

  const { data, loading } = useSubscription(NEW_MESSAGE);

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h2>Message List</h2>
      <ul>
        {data?.newMessage && (
          <li>
            <strong>{data.newMessage.content}</strong>
          </li>
        )}
      </ul>
    </div>
  );
};

export default MessageList;
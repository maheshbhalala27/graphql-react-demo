import React, { useState } from "react";
import { useMutation } from "@apollo/client";
import { signup_User } from "../store/query";

const Create = () => {
  // initialValues
  const initialValues = {
    FirstName: "",
    lastName: "",
    email: "",
    password: "",
  };

  // ** State
  const [formHandler, setFormHandler] = useState(initialValues);

  const [signupUser, { data, loading, error }] = useMutation(signup_User, {});


  const handelChange = (e) => {
    const { name, value } = e.target;

    setFormHandler({
      ...formHandler,
      [name]: value,
    });
  };


  // if (loading) return "Submitting...";
  // if (error) return `Submission error! ${error.message}`;

  return (
    <>
      <form
        style={{ margin: '25px', display: "flex", alignItems: "center", justifyContent: "center" }}
        onSubmit={(e) => {
          e.preventDefault();
          if (formHandler.FirstName !== '' && formHandler.FirstName !== null
            && formHandler.lastName !== "" && formHandler.lastName !== null
            && formHandler.email !== "" && formHandler.email !== null
            && formHandler.password !== "" && formHandler.password !== null
          ) {

            signupUser({ variables: formHandler }).then((res) =>{
              setFormHandler(initialValues)
            });

          }
        }}
      >
        <div>
          <label htmlFor="FirstName">First Name</label> <br />
          <input
            type="text"
            name="FirstName"
            id="FirstName"
            value={formHandler.FirstName}
            onChange={(e) => handelChange(e)}
          />
          <br></br>
          <label htmlFor="lastName">Last Name</label> <br />
          <input
            type="text"
            name="lastName"
            id="lastName"
            value={formHandler.lastName}
            onChange={(e) => handelChange(e)}
          />
          <br></br>
          <label htmlFor="email">Email</label> <br />
          <input
            type="email"
            name="email"
            id="email"
            value={formHandler.email}
            onChange={(e) => handelChange(e)}
          />
          <br></br>
          <label htmlFor="password">Password</label> <br />
          <input
            type="password"
            name="password"
            id="password"
            value={formHandler.password}
            onChange={(e) => handelChange(e)}
          />
          <br></br>
          <div style={{ margin: '15px', display: "flex", alignItems: "center", justifyContent: "center" }}>

            <button type="submit">Add Todo</button>
          </div>
        </div>
      </form>
    </>
  );
};

export default Create;

import React from "react";
import Header from "../component/Header";
import { Outlet } from "react-router-dom";
import Footer from "../component/Footer";

const DefaultLayout = () => {
  return (
    <div className="default-layout">
      <Header />
      <div className="outlet-default ">
        <Outlet />
      </div>
      <Footer />
    </div>
  );
};

export default DefaultLayout;

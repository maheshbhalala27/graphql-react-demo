import React from 'react';
import { render } from 'react-dom';
import { ApolloProvider } from '@apollo/client';
import { BrowserRouter } from 'react-router-dom';
import { MockedProvider } from '@apollo/client/testing';
import { client } from './store/client';
import App from './App';


test('renders without crashing', () => {
    const div = document.createElement('div');
    render(
      <MockedProvider mocks={[]} addTypename={false}>
        <ApolloProvider client={client}>
          <BrowserRouter>
            <App />
          </BrowserRouter>
        </ApolloProvider>
      </MockedProvider>,
      div
    );
  });

  // import React from 'react';
// import { render, screen } from '@testing-library/react';
// import { MemoryRouter } from 'react-router-dom';
// import App from './App';

// test('renders navigation links', () => {
//   render(
//     <MemoryRouter>
//       <App />
//     </MemoryRouter>
//   );

//   const homeLink = screen.getByText("Home");
//   const createLink = screen.getByText("Create");
//   const subscriptionLink = screen.getByText("Subscription");
//   const contentLink = screen.getByText("Content");

//   expect(homeLink).toBeInTheDocument();
//   expect(createLink).toBeInTheDocument();
//   expect(subscriptionLink).toBeInTheDocument();
//   expect(contentLink).toBeInTheDocument();
// });
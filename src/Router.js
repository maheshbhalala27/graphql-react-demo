import React from 'react'
import { Route, Routes } from 'react-router-dom'
import Subscription from './page/Subscription'
import Create from './page/Create'
import Home from './page/Home'
import MessageList from './page/MessageList'
import DefaultLayout from './layout'
// import GetSubscription from './GetSubscription'

const Router = () => {
  return (
    <>
         <Routes>
          <Route path="/" element={<DefaultLayout />} > 
            {/* Home */}
          <Route path="/" element={<Home />} /> 
          {/* Create */}
          <Route path="/create" element={<Create />} />
          {/* Create Subscription*/}
          <Route path="/subscription" element={<Subscription />} />
          {/* Get Subscription */}
          <Route path="/get-content" element={<MessageList />} />
        </Route>
        </Routes>
    </>
  )
}

export default Router
import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import Subscription from '../Subscription';
import {NEW_MESSAGE} from '../store/query';
import { useSubscription } from '@apollo/client';

jest.mock('@apollo/client', () => ({
    gql: jest.fn(),
    useSubscription: jest.fn(),
  }));
  
  jest.mock('../store/client', () => ({
    client: {
      mutate: jest.fn(),
    },
  }));

// test('renders list items for each message', () => {
//   const messages = [
//     { id: '1', content: 'Message 1' },
//     { id: '2', content: 'Message 2' },
//     { id: '3', content: 'Message 3' },
//   ];

//   // Render the Subscription component with the provided messages
//   const { getByTestId } = render(<Subscription />, {
//     initialState: { messages },
//   });

//   // Assert that the correct number of list items are rendered
//   const testMessages = getByTestId('test-message');
//   expect(testMessages.length).toBe(messages.length);

//   // Assert that each list item contains the expected message content
//   testMessages.forEach((testMessage, index) => {
//     expect(testMessage).toHaveTextContent(messages[index].content);
//   });
// });

    test('submits the form and calls postMessage', () => {
      const postMessageMock = jest.fn();
  
      useSubscription.mockReturnValue({
        data: null,
        error: null,
      });
  
      const { getByLabelText, getByText } = render(<Subscription />);
  
      const contentInput = getByLabelText('Content');
      const submitButton = getByText('Send');
  
      const content = 'Test message';
      fireEvent.change(contentInput, { target: { value: content } });
      fireEvent.click(submitButton);
  
      // expect(postMessageMock).toHaveBeenCalledTimes(1);
      // expect(postMessageMock).toHaveBeenCalledWith(content);
      expect(contentInput.value).toBe('');
    });
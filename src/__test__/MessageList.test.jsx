import React from 'react';
import { render, waitFor, screen } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import MessageList from '../MessageList';
import { NEW_MESSAGE } from '../store/query';

// Mock the Apollo subscription response
const mockData = {
    newMessage: {
      id: '1',
      content: 'Test Message',
    },
  };
  
  const mocks = [
    {
      request: {
        query: NEW_MESSAGE,
      },
      result: {
        data: mockData,
      },
    },
  ];

describe('MessageList component', () => {
 it('renders loading state initially', async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <MessageList />
      </MockedProvider>
    );

    expect(screen.getByText('Loading...')).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.queryByText('Loading...')).toBeNull();
    });
  });

  it('renders the received message', async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <MessageList />
      </MockedProvider>
    );

    await waitFor(() => {
      expect(screen.getByText('Message List')).toBeInTheDocument();
      expect(screen.getByText('Test Message')).toBeInTheDocument();
    });
  });
});
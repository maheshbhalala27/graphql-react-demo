import React from 'react';
import { render, screen } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import Home from '../Home';
import { GET_ALL_USER } from '../store/query';

const mocks = [
    {
      request: {
        query: GET_ALL_USER,
      },
      result: {
        data: {
          userData: [
            {
              id: 1,
              FirstName: 'John',
              lastName: 'Doe',
            },
            {
              id: 2,
              FirstName: 'Jane',
              lastName: 'Smith',
            },
          ],
        },
      },
    },
  ];

test('renders user data when loaded successfully', async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Home />
      </MockedProvider>
    );
  
    // Assert loading state
    expect(screen.getByText(/loading/i)).toBeInTheDocument();
  
    // Wait for the query to resolve
    await screen.findByText(/Id : 1/i);
  
    // Assert rendered user data
    expect(screen.getByText(/Id : 1/i)).toBeInTheDocument();
    expect(screen.getByText(/First Name : John/i)).toBeInTheDocument();
    expect(screen.getByText(/Last Name : Doe/i)).toBeInTheDocument();
  
    expect(screen.getByText(/Id : 2/i)).toBeInTheDocument();
    expect(screen.getByText(/First Name : Jane/i)).toBeInTheDocument();
    expect(screen.getByText(/Last Name : Smith/i)).toBeInTheDocument();
  });
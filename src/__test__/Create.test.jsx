import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import Create from '../Create';
import { gql, useMutation } from '@apollo/client';

const mocks = [
    {
      request: {
        query: gql`
          mutation signupUser(
            $FirstName: String!
            $lastName: String!
            $email: String!
            $password: String!
          ) {
            signupUser(
              FirstName: $FirstName
              lastName: $lastName
              email: $email
              password: $password
            ) {
              password
              FirstName
              lastName
              email
              id
            }
          }
        `,
        variables: {
          FirstName: 'John',
          lastName: 'Doe',
          email: 'john.doe@example.com',
          password: 'password123',
        },
      },
      result: {
        data: {
          signupUser: {
            id: '1',
            FirstName: 'John',
            lastName: 'Doe',
            email: 'john.doe@example.com',
            password: '',
          },
        },
      },
    },
  ];

test('submits the form and calls the signupUser mutation', async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Create />
      </MockedProvider>
    );

      // const userList = await waitFor(() => screen.getByTestId('user-list'));
  
    const firstNameInput = screen.getByLabelText(/First Name/i);
    const lastNameInput = screen.getByLabelText(/Last Name/i);
    const emailInput = screen.getByLabelText(/Email/i);
    const passwordInput = screen.getByLabelText(/Password/i);
    const submitButton = screen.getByRole('button', { name: /Add Todo/i });
  
    fireEvent.change(firstNameInput, { target: { value: 'John' } });
    fireEvent.change(lastNameInput, { target: { value: 'Doe' } });
    fireEvent.change(emailInput, { target: { value: 'john.doe@example.com' } });
    fireEvent.change(passwordInput, { target: { value: 'password123' } });
  
    fireEvent.click(submitButton);
  
    // Wait for the mutation to resolve
    // await waitFor(() => screen.getByText(/Submitting.../i));
  
    // Assert successful submission
    expect(firstNameInput).toBeInTheDocument();
    expect(lastNameInput).toBeInTheDocument();
    expect(emailInput).toBeInTheDocument();
    expect(passwordInput).toBeInTheDocument();
  });
import { gql } from "@apollo/client";

export const signup_User = gql`
  mutation signupUser(
    $FirstName: String!
    $lastName: String!
    $email: String!
    $password: String!
  ) {
    signupUser(
      FirstName: $FirstName
      lastName: $lastName
      email: $email
      password: $password
    ) {
      password
      FirstName
      lastName
      email
      id
    }
  }
`;

export const GET_ALL_USER = gql`
  query user {
    userData {
      id
      FirstName
      lastName
      email
    }
  }
`;

export const NEW_MESSAGE = gql`
  subscription {
    newMessage {
      id
      content
    }
  }
`;

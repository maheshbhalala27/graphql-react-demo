import { ApolloClient, HttpLink, InMemoryCache, split } from "@apollo/client";
import { getMainDefinition } from "@apollo/client/utilities";
import { GraphQLWsLink } from "@apollo/client/link/subscriptions";
import { createClient } from "graphql-ws";

const baseURL = process.env.REACT_APP_API_BASE_URL

export const wsLink = new GraphQLWsLink(
  createClient({
    url: baseURL, // backend link, check backend console for link
  })
);

export const httpLink = new HttpLink({
  uri: baseURL
});


export const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    );
  },
  wsLink,
  httpLink,
);


export const client = new ApolloClient({
  link:splitLink,
  cache: new InMemoryCache(),
  });